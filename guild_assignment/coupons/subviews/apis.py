
from django.shortcuts import render
import os
from django.core.files import File

import hashlib, uuid, pytz, datetime, json
from datetime import datetime, timedelta

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import Template, Context
from django.template.loader import get_template
from django.contrib.auth import login as django_login
from django.db import transaction
from django.urls import reverse
from django.db.models import Q, Subquery, Max
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.apps import apps
from django.core.serializers import serialize

from rest_framework import status , generics
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny, BasePermission
from rest_framework.authtoken.models import Token

from utils.api_handle_errors import handle_errors, check_platform_user

from coupons.models import *

from coupons.serializers import *
from users.serializers import *

from coupons.functions import *
from guild_assignment.settings import BASE_DIR




class PublishedDealsAPIView(generics.GenericAPIView):
	authentication_classes = (TokenAuthentication, BasicAuthentication, SessionAuthentication)
	permission_classes = (IsAuthenticated,)
	serializer_class = VerifyUserInputSerializer

	@transaction.atomic
	def post(self, request, format=None):

		response = {}
		response["data"] = []

		platform_user, response = check_platform_user(request)
		if not platform_user:
			return Response(response, status=status.HTTP_200_OK)

		all_published_deals = Coupon.objects.filter(published=True).order_by("-cLastUpdated")

		all_published_deals_data = CouponGetSerializer(all_published_deals, many = True).data
		total_count = len(all_published_deals)

		response["data"] = all_published_deals_data
		response["total_count"] = total_count
		response["result"] = 1

		return Response(response, status=status.HTTP_200_OK)


class ActiveDealsAPIView(generics.GenericAPIView):
	authentication_classes = (TokenAuthentication, BasicAuthentication, SessionAuthentication)
	permission_classes = (IsAuthenticated,)
	serializer_class = VerifyUserInputSerializer

	@transaction.atomic
	def post(self, request, format=None):

		response = {}
		response["data"] = []

		platform_user, response = check_platform_user(request)
		if not platform_user:
			return Response(response, status=status.HTTP_200_OK)

		all_active_deals = Coupon.objects.filter(published=True, dtEndDate__gt = datetime.now()) 

		all_active_deals_data = CouponGetSerializer(all_active_deals, many = True).data
		total_count = len(all_active_deals)

		response["data"] = all_active_deals_data
		response["total_count"] = total_count
		response["result"] = 1

		return Response(response, status=status.HTTP_200_OK)



class PublishDealAPIView(generics.GenericAPIView):
	authentication_classes = (TokenAuthentication, BasicAuthentication, SessionAuthentication)
	permission_classes = (IsAuthenticated,)
	serializer_class = PublishDealInputSerializer

	@transaction.atomic
	def post(self, request, format=None):

		serializer = PublishDealInputSerializer(data=request.data)
		response = {}

		if serializer.is_valid():

			pk = serializer.data['pk']

			corresponding_deal = Coupon.objects.get(pk = pk)

			if corresponding_deal.published:
				corresponding_deal.published = False
				CouponLog.objects.create(coupon = corresponding_deal, unpublished=True, created_by = request.user)
				response["message"] = ["Coupon Successfully UnPublished"]
			else:
				corresponding_deal.published = True
				CouponLog.objects.create(coupon = corresponding_deal, published=True, created_by = request.user)
				response["message"] = ["Coupon Successfully Published"]

			corresponding_deal.save()

			response["result"] = 1
			
		else:
			response["result"] = 0
			response["message"] = ["Please check the input correctly"]

		return Response(response, status=status.HTTP_200_OK)


class AllDealsAPIView(generics.GenericAPIView):
	authentication_classes = (TokenAuthentication, BasicAuthentication, SessionAuthentication)
	permission_classes = (IsAuthenticated,)
	serializer_class = AllDealsGetInputSerializer

	@transaction.atomic
	def post(self, request, format=None):
		serializer = AllDealsGetInputSerializer(data=request.data)
		response = {}

		if serializer.is_valid():

			page = serializer.data['page']

			platform_user, response = check_platform_user(request)
			if not platform_user:
				return Response(response, status=status.HTTP_200_OK)

			all_deals = Coupon.objects.all() 

			total_count = len(all_deals)

			all_deals = paginate(all_deals, page) 

			all_deals_data = CouponGetSerializer(all_deals, many = True).data
			
			response["data"] = all_deals_data
			response["total_count"] = total_count
			response["result"] = 1

		else:
			response["message"] = ["Input Not Valid"]
			response["result"] = 0
		return Response(response, status=status.HTTP_200_OK)




class StartEditDealAPIView(generics.GenericAPIView):
	authentication_classes = (TokenAuthentication, BasicAuthentication, SessionAuthentication)
	permission_classes = (IsAuthenticated,)
	serializer_class = StartEditDealInputSerializer

	@transaction.atomic
	def post(self, request, format=None):
		serializer = StartEditDealInputSerializer(data=request.data)
		response = {}

		if serializer.is_valid():

			pk = serializer.data['pk']

			platform_user, response = check_platform_user(request)
			if not platform_user:
				return Response(response, status=status.HTTP_200_OK)

			corresponding_deal = Coupon.objects.get(pk = pk)

			now = datetime.now()
			earlier = now - timedelta(minutes=5)

			if CouponEditStart.objects.filter(coupon = corresponding_deal, start_time__range=(earlier,now)).exclude(created_by=request.user).exists():
				response["result"] = 0
				response["message"] = ["Somebody else is editing the same deal. Please wait for your turn"]

			else:
				corresponding_edit_start = CouponEditStart(coupon = corresponding_deal, start_time=now)
				corresponding_edit_start.created_by = request.user
				corresponding_edit_start.save()			
				response["result"] = 1
				response["message"] = ["Editing of the deal has started"]
		else:
			response["result"] = 0
			response["message"] = ["Please check the input correctly"]


		return Response(response, status=status.HTTP_200_OK)




class EditDealAPIView(generics.GenericAPIView):
	authentication_classes = (TokenAuthentication, BasicAuthentication, SessionAuthentication)
	permission_classes = (IsAuthenticated,)
	serializer_class = EditDealInputSerializer

	@transaction.atomic
	def post(self, request, format=None):
		serializer = EditDealInputSerializer(data=request.data)
		response = {}

		if serializer.is_valid():

			pk = serializer.data['pk']
			cLabel = serializer.data['cLabel']
			cDescription = serializer.data['cDescription']

			platform_user, response = check_platform_user(request)
			if not platform_user:
				return Response(response, status=status.HTTP_200_OK)

			corresponding_deal = Coupon.objects.get(pk = pk)

			now = datetime.now()
			earlier = now - timedelta(minutes=5)

			if CouponEditStart.objects.filter(coupon = corresponding_deal, start_time__range=(earlier,now)).exclude(created_by = request.user).exists():
				response["result"] = 0
				response["message"] = ["Somebody else is editing the same deal. Please wait for your turn"]
				return Response(response, status=status.HTTP_200_OK)


			corresponding_deal.cLabel = cLabel
			corresponding_deal.cDescription = cDescription
				
			corresponding_deal.save()

			response["result"] = 1
			response["message"] = ["Deal has been successfully edited"]
			
		else:
			response["result"] = 0
			response["message"] = ["Please check the input correctly"]


		return Response(response, status=status.HTTP_200_OK)



