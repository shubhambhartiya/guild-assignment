
from django.shortcuts import render
import os
from django.core.files import File

import hashlib, uuid, pytz, datetime, json
from datetime import datetime, timedelta

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import Template, Context
from django.template.loader import get_template
from django.contrib.auth import login as django_login
from django.db import transaction
from django.urls import reverse
from django.db.models import Q, Subquery, Max
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.apps import apps
from django.core.serializers import serialize

from rest_framework import status , generics
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny, BasePermission
from rest_framework.authtoken.models import Token

from utils.api_handle_errors import handle_errors, check_platform_user

from coupons.models import *

from guild_assignment.settings import BASE_DIR



def prefill_database():
	print(BASE_DIR)

	f = open(BASE_DIR+"/guild_assignment/deals.json") 
  
	data = json.load(f)
	print(data)

	for an_object in data:

		if Coupon.objects.filter(nCouponID = an_object["nCouponID"]).exists():
			corresponding_coupon = Coupon.objects.get(nCouponID = an_object["nCouponID"])
		else:
			corresponding_coupon = Coupon.objects.create(nCouponID = an_object["nCouponID"])


		corresponding_coupon.nCouponID = an_object["nCouponID"]
		corresponding_coupon.cMerchant = an_object["cMerchant"]
		corresponding_coupon.nMerchantID = an_object["nMerchantID"]
		corresponding_coupon.nMasterMerchantID = an_object["nMasterMerchantID"]
		corresponding_coupon.cNetwork = an_object["cNetwork"]
		corresponding_coupon.cStatus = an_object["cStatus"]
		corresponding_coupon.cLabel = an_object["cLabel"]
		corresponding_coupon.cImage = an_object["cImage"]
		corresponding_coupon.cRestrictions = an_object["cRestrictions"]
		corresponding_coupon.cCode = an_object["cCode"]
		corresponding_coupon.dtStartDate = an_object["dtStartDate"]
		corresponding_coupon.dtEndDate = an_object["dtEndDate"]
		corresponding_coupon.cLastUpdated = an_object["cLastUpdated"]
		corresponding_coupon.cCreated = an_object["cCreated"]
		corresponding_coupon.cAffiliateURL = an_object["cAffiliateURL"]
		corresponding_coupon.cDirectURL = an_object["cDirectURL"]
		corresponding_coupon.cSkimlinksURL = an_object["cSkimlinksURL"]
		corresponding_coupon.cFreshReachURL = an_object["cFreshReachURL"]
		corresponding_coupon.cFMTCURL = an_object["cFMTCURL"]
		corresponding_coupon.cPixelHTML = an_object["cPixelHTML"]
		corresponding_coupon.fSalePrice = an_object["fSalePrice"]
		corresponding_coupon.fWasPrice = an_object["fWasPrice"]
		corresponding_coupon.fDiscount = an_object["fDiscount"]
		corresponding_coupon.nPercent = an_object["nPercent"]
		corresponding_coupon.fThreshold = an_object["fThreshold"]
		corresponding_coupon.fRating = an_object["fRating"]
		corresponding_coupon.bStarred = an_object["bStarred"]
		corresponding_coupon.nLinkID = an_object["nLinkID"]

		corresponding_coupon.save()

		corresponding_coupon_a_types = corresponding_coupon.aTypes.all()
		aTypes = an_object["aTypes"]
		for x in aTypes:
			if not x in corresponding_coupon_a_types:
				if not CouponType.objects.filter(name = x).exists():
					corresponding_a_type = CouponType.objects.create(name = x)
				else:
					corresponding_a_type = CouponType.objects.get(name = x)

				corresponding_coupon.aTypes.add(corresponding_a_type)
				corresponding_coupon.save()

		print(corresponding_coupon.nCouponID)
		corresponding_coupon_aCategoriesV2 = corresponding_coupon.aCategoriesV2.all()
		aCategoriesV2 = an_object["aCategoriesV2"]
		if isinstance(aCategoriesV2, list):
			print("true")
			for x in aCategoriesV2:
				if not x in corresponding_coupon_aCategoriesV2:
					if not CouponCategories.objects.filter(nCategoryID = x["nCategoryID"]).exists():
						corresponding_coupon_category = CouponCategories.objects.create(nCategoryID = x["nCategoryID"])
					else:
						corresponding_coupon_category = CouponCategories.objects.get(nCategoryID = x["nCategoryID"])

					corresponding_coupon_category.cName = x["cName"]
					corresponding_coupon_category.nParentID = x["nParentID"]
					corresponding_coupon_category.bRestricted = x["bRestricted"]
					corresponding_coupon_category.save()



					corresponding_coupon.aCategoriesV2.add(corresponding_coupon_category)
					corresponding_coupon.save()



		


def save_and_publish(request):
	corresponding_object = Coupon.objects.get(pk = request.GET["pk"])
	corresponding_object.published = True
	corresponding_object.save()

	print("/admin/coupons/coupon/=/?q="+str(corresponding_object.pk))

	return redirect("/admin/coupons/coupon/?pk="+str(corresponding_object.pk))