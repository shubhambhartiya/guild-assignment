PAGE_SIZE = 20




def paginate(items, page):
	count      = len(items)
	page_count = count/PAGE_SIZE + 1

	page_no = int(page) - 1
	offset  = page_no * PAGE_SIZE

	return items[offset:offset+PAGE_SIZE]


