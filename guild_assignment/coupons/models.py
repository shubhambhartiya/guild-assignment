from django.db import models
from django.contrib.auth.models import User


class Brand(models.Model):
	""" Coupon Types """
	name       = models.CharField(max_length=250, null=True)

	created_at = models.DateTimeField(auto_now_add=True, null=True)
	updated_at = models.DateTimeField(auto_now=True)


	def __str__(self):
		return "{}".format(self.name)

	class Meta:
		verbose_name_plural = "Brands"
		db_table = "brands"


class Local(models.Model):
	""" Coupon Types """
	name       = models.CharField(max_length=250, null=True)

	created_at = models.DateTimeField(auto_now_add=True, null=True)
	updated_at = models.DateTimeField(auto_now=True)


	def __str__(self):
		return "{}".format(self.name)

	class Meta:
		verbose_name_plural = "Local"
		db_table = "local"


class CouponType(models.Model):
	""" Coupon Types """
	name       = models.CharField(max_length=250, null=True)

	created_at = models.DateTimeField(auto_now_add=True, null=True)
	updated_at = models.DateTimeField(auto_now=True)


	def __str__(self):
		return "{}".format(self.name)

	class Meta:
		verbose_name_plural = "Coupon Types"
		db_table = "coupon_type"


class CouponCategories(models.Model):
	""" Coupon Categories """
	cName       	  = models.CharField(max_length=250, null=True)
	nCategoryID       = models.IntegerField(blank=True, null=True)
	nParentID         = models.IntegerField(blank=True, null=True)
	bRestricted       = models.IntegerField(blank=True, null=True)

	created_at 		  = models.DateTimeField(auto_now_add=True, null=True)
	updated_at 		  = models.DateTimeField(auto_now=True)

	def __str__(self):
		return "{}".format(self.cName) 

	class Meta:
		verbose_name_plural = "Coupon Categories"
		db_table = "coupon_categories"




class Coupon(models.Model):
	""" Coupon Categories """
	nCouponID           = models.IntegerField(blank=True, null=True)
	cMerchant           = models.CharField(max_length=250, blank=True, null=True)
	cDescription        = models.TextField(blank=True, null=True)
	nMerchantID         = models.IntegerField(blank=True, null=True)
	nMasterMerchantID   = models.IntegerField(blank=True, null=True)
	cNetwork       		= models.CharField(max_length=250, blank=True, null=True)
	cStatus        		= models.CharField(max_length=250, blank=True, null=True)
	cLabel         		= models.CharField(max_length=250, blank=True, null=True)
	cImage				= models.FileField(upload_to = 'coupon_image', blank=True, null=True)
	cRestrictions		= models.CharField(max_length=250, blank=True, null=True)
	cCode  				= models.CharField(max_length=250, blank=True, null=True)

	dtStartDate			= models.DateTimeField(blank=True, null=True)
	dtEndDate			= models.DateTimeField(blank=True, null=True)
	cLastUpdated		= models.DateTimeField(auto_now=True)
	cCreated			= models.DateTimeField(auto_now_add=True, null=True)


	cAffiliateURL		= models.CharField(max_length=250, blank=True, null=True)
	cDirectURL			= models.CharField(max_length=250, blank=True, null=True)
	cSkimlinksURL		= models.CharField(max_length=250, blank=True, null=True)
	cFreshReachURL		= models.CharField(max_length=250, blank=True, null=True)
	cFMTCURL			= models.CharField(max_length=250, blank=True, null=True)
	cPixelHTML			= models.CharField(max_length=250, blank=True, null=True)

	aTypes  			= models.ManyToManyField(CouponType, blank=True)

	fSalePrice			= models.FloatField(default=0.0)
	fWasPrice			= models.FloatField(default=0.0)
	fDiscount			= models.FloatField(default=0.0)
	nPercent			= models.FloatField(default=0.0)
	fThreshold			= models.FloatField(default=0.0)
	fRating				= models.FloatField(default=0.0)
	bStarred			= models.FloatField(default=0.0)
	aBrands				= models.ManyToManyField(Brand, blank=True)
	aLocal				= models.ManyToManyField(Local, blank=True)
	nLinkID				= models.IntegerField(blank=True, null=True)

	published    		= models.BooleanField(default = False)

	aCategoriesV2       = models.ManyToManyField(CouponCategories, blank=True)

	def __str__(self):
		return "{}".format(self.nCouponID)

	class Meta:
		verbose_name_plural = "Coupons"
		db_table = "coupon"


class CouponLog(models.Model):
	""" Coupon Categories """
	coupon          	= models.ForeignKey(Coupon, blank=True, null=True,  on_delete=models.CASCADE,)

	published           = models.BooleanField(default = False)
	unpublished 		= models.BooleanField(default = False)

	created_by   		= models.ForeignKey(User, null=True, blank=True,  on_delete=models.CASCADE,)

	created_at 			= models.DateTimeField(auto_now_add=True, null=True)
	updated_at 			= models.DateTimeField(auto_now=True)


	def __str__(self):
		return "{}".format(self.coupon)

	class Meta:
		verbose_name_plural = "CouponsLog"
		db_table = "coupon_log"



class CouponEditStart(models.Model):
	""" Coupon Categories """
	coupon          	= models.ForeignKey(Coupon, blank=True, null=True,  on_delete=models.CASCADE,)

	start_time          = models.DateTimeField(auto_now_add=True, null=True)

	created_by   		= models.ForeignKey(User, null=True, blank=True,  on_delete=models.CASCADE,)

	created_at 			= models.DateTimeField(auto_now_add=True, null=True)
	updated_at 			= models.DateTimeField(auto_now=True)


	def __str__(self):
		return "{}".format(self.coupon)

	class Meta:
		verbose_name_plural = "CouponEditStart"
		db_table = "coupon_edit_start"
