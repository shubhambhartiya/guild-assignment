from django.conf.urls import url, include
from .views import *

urlpatterns = [
	url(r'^save_and_publish/', save_and_publish, name="save_and_publish"),

	url(r'^get_published_deals/', PublishedDealsAPIView.as_view(), name="get_published_deals"),
	url(r'^get_active_deals/', ActiveDealsAPIView.as_view(), name="get_active_deals"),

	url(r'^get_all_deals/', AllDealsAPIView.as_view(), name="get_all_deals"),

	url(r'^publish_deal/', PublishDealAPIView.as_view(), name="publish_deal"),

	url(r'^start_edit_deal/', StartEditDealAPIView.as_view(), name="start_edit_deal"),

	url(r'^edit_deal/', EditDealAPIView.as_view(), name="edit_Deal"),
]





