# Generated by Django 2.2 on 2020-05-03 23:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coupons', '0006_auto_20200504_0432'),
    ]

    operations = [
        migrations.AlterField(
            model_name='couponeditstart',
            name='start_time',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
    ]
