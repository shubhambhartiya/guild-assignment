from rest_framework import serializers
from django.contrib.auth.models import User, Group
from rest_framework.authtoken.models import Token
from .models import *



class CouponTypeGetSerializer(serializers.ModelSerializer):    

	class Meta:
		model  = CouponType
		fields = ('pk', 'name',)



class CouponCategoriesGetSerializer(serializers.ModelSerializer):    

	class Meta:
		model  = CouponCategories
		fields = ('pk', 'cName', 'nCategoryID', 'nParentID', 'bRestricted',)



class CouponGetSerializer(serializers.ModelSerializer):   
	aCategoriesV2 = CouponCategoriesGetSerializer(many=True)
	aTypes		  = CouponTypeGetSerializer(many=True)

	class Meta:
		model  = Coupon
		fields = ('pk', 'nCouponID', 'cLabel', 'cMerchant', 'aCategoriesV2', 'cNetwork', 'aTypes', 'dtStartDate', 'dtEndDate', 'cStatus', 'published', 'cDescription')



class PublishDealInputSerializer(serializers.ModelSerializer):   
	pk   = serializers.IntegerField(required=True, help_text="Corresponding pk of the deal which needs to be published")

	class Meta:
		model  = Coupon
		fields = ('pk',)


class AllDealsGetInputSerializer(serializers.ModelSerializer):   
	page   = serializers.IntegerField(required=True, help_text="Corresponding page number")

	class Meta:
		model  = Coupon
		fields = ('page',)




class EditDealInputSerializer(serializers.ModelSerializer):   
	cLabel   	  = serializers.CharField(required=True, max_length=250)
	cDescription  = serializers.CharField(required=True, max_length=100)
	pk   		  = serializers.IntegerField(required=True, help_text="Corresponding pk of the deal which needs to be edited")

	class Meta:
		model  = Coupon
		fields = ('pk', 'cLabel', 'cDescription')








class StartEditDealInputSerializer(serializers.ModelSerializer):   
	pk   = serializers.IntegerField(required=True, help_text="Corresponding pk of the deal which needs to be published")

	class Meta:
		model  = Coupon
		fields = ('pk',)

