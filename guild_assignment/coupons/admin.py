from django.contrib import admin

from django.utils.html import format_html

from nested_admin import NestedModelAdmin, NestedInlineModelAdmin
from nested_admin import NestedModelAdmin

from .models import *


class BrandAdmin(NestedModelAdmin):
	model = Brand

	list_display 	= ['name', 'created_at', 'updated_at']
    
admin.site.register(Brand, BrandAdmin)


class LocalAdmin(NestedModelAdmin):
	model = Local

	list_display 	= ['name', 'created_at', 'updated_at']
    
admin.site.register(Local, LocalAdmin)

class CouponTypeAdmin(NestedModelAdmin):
	model = CouponType

	list_display 	= ['name', 'created_at', 'updated_at']
    
admin.site.register(CouponType, CouponTypeAdmin)


class CouponCategoriesAdmin(NestedModelAdmin):
	model = CouponCategories

	list_display 	= ['cName', 'nCategoryID', 'nParentID', 'bRestricted', 'created_at', 'updated_at']
    
admin.site.register(CouponCategories, CouponCategoriesAdmin)


class CouponAdmin(NestedModelAdmin):
	model = Coupon

	list_display 	= ['coupon_ID', 'coupon_name', 'merchant', 'category', 'network', 'types', 'start_date', 'end_date', 'status', 'published', 'publish',]
	readonly_fields = ['cLastUpdated', 'cCreated']
	search_fields   = ['pk', 'cLabel', 'nCouponID']

	def coupon_ID(self, obj):
		return format_html('{coupon_id}', coupon_id=obj.nCouponID,)

	def coupon_name(self, obj):
		return format_html('{coupon_name}', coupon_name=obj.cLabel,)

	def merchant(self, obj):
		return format_html('{merchant}', merchant=obj.cMerchant,)

	def category(self, obj):
		categories = ", \n".join([p.cName for p in obj.aCategoriesV2.all()])
		return categories

	def network(self, obj):
		return format_html('{network}', network=obj.cNetwork,)

	def types(self, obj):
		type_values = ", \n".join([p.name for p in obj.aTypes.all()])
		return type_values

	def start_date(self, obj):
		return format_html('{start_date}', start_date=obj.dtStartDate.date(),)

	def end_date(self, obj):
		return format_html('{end_date}', end_date=obj.dtEndDate.date(),)

	def status(self, obj):
		return format_html('{status}', status=obj.cStatus,)

	def publish(self, obj):
		return format_html('<button><a href="/coupons/save_and_publish?pk={pk}">Save & Publish</a></button>', pk=obj.pk,)

admin.site.register(Coupon, CouponAdmin)





class CouponLogAdmin(NestedModelAdmin):
	model = CouponLog

	list_display 	= ['coupon', 'published', 'unpublished', 'created_by', 'created_at', 'updated_at',]

admin.site.register(CouponLog, CouponLogAdmin)



class CouponEditStartAdmin(NestedModelAdmin):
	model = CouponEditStart

	list_display 	= ['coupon', 'start_time', 'created_by', 'created_at', 'updated_at',]


admin.site.register(CouponEditStart, CouponEditStartAdmin)


 









