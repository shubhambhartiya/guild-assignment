from users.models import PlatformUser

from datetime import datetime, timedelta

def handle_errors(serializer_errors):
	response = {}
	response['errors'] = [{}]
	for key,value in serializer_errors.items():
		response["errors"][0][key] = value[0]
	return response

def check_platform_user(request):
	response      = {}
	platform_user = ''
	try:
		platform_user = PlatformUser.objects.get(user = request.user, is_deleted=False)
	except PlatformUser.DoesNotExist:
		response['result'] = 0
		response['errors'] = ['Invalid User..!']
	finally:
		return platform_user, response