from rest_framework import serializers
from django.contrib.auth.models import User, Group
from rest_framework.authtoken.models import Token
from .models import *



class VerifyUserInputSerializer(serializers.ModelSerializer):
	
	class Meta:
		model  = PlatformUser
		fields = ()



class LoginAsInputSerializer(serializers.ModelSerializer):
	name = serializers.CharField(required=True, max_length=100, help_text = "Super Admin / Content Writer")
	class Meta:
		model  = UserType
		fields = ('name',)
