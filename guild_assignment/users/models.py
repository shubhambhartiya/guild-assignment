from django.db import models
from django.contrib.auth.models import User


class UserType(models.Model):
	""" Differnet User Types """
	name       = models.CharField(max_length=250, null=True)
	priority   = models.IntegerField(default = 0)

	is_deleted = models.BooleanField(default=False)
	created_at = models.DateTimeField(auto_now_add=True, null=True)
	updated_at = models.DateTimeField(auto_now=True)

	def __str__(self):
		return "{}".format(self.name)

	class Meta:
		verbose_name_plural = "User Types"
		db_table = "user_type"


class PlatformUser(models.Model):
	""" Register User details reside here """
	user 		= models.ForeignKey(User, on_delete=models.CASCADE, null = True, blank = True, related_name = "internal_user")
	uid         = models.CharField(max_length=25, null=True, help_text="UID")

	first_name  = models.CharField(max_length=100, null=True, help_text="user first name")
	last_name   = models.CharField(max_length=100, null=True, help_text="user middle name", blank=True)
	
	mobile		= models.CharField(max_length=10, null=True, help_text="user mobile number", unique=True)
	email		= models.CharField(max_length=100, null=True, help_text="user email id", unique=True)

	designation = models.CharField(max_length=100, null=True, help_text="designation", blank=True)

	user_type   = models.ForeignKey(UserType, null = True, on_delete=models.CASCADE,)
	
	created_by  = models.ForeignKey(User, on_delete=models.CASCADE, null = True, blank = True, related_name = "staff_created_by")
	created_at	= models.DateTimeField(auto_now_add=True, blank=True, null=True)
	updated_at	= models.DateTimeField(auto_now=True, blank=True, null=True)

	is_deleted 	= models.BooleanField(default=False, help_text="activate/deactivate user")

	def __str__(self):
		return "{}-{}".format(self.first_name, self.mobile)

	class Meta:
		verbose_name_plural = 'Platform Users'
		db_table			= 'platform_user'


