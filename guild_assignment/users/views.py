
from django.shortcuts import render
import os
from django.core.files import File

import hashlib, uuid, pytz, datetime, json
from datetime import datetime, timedelta

from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import Template, Context
from django.template.loader import get_template
from django.contrib.auth import login as django_login
from django.db import transaction
from django.urls import reverse
from django.db.models import Q, Subquery, Max
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from django.apps import apps
from django.core.serializers import serialize

from rest_framework import status , generics
from rest_framework.response import Response
from rest_framework.authentication import TokenAuthentication, BasicAuthentication, SessionAuthentication
from rest_framework.permissions import IsAuthenticated, AllowAny, BasePermission
from rest_framework.authtoken.models import Token

from utils.api_handle_errors import handle_errors, check_platform_user

from coupons.models import *

from coupons.serializers import *
from users.serializers import *

from coupons.functions import *
from guild_assignment.settings import BASE_DIR




class LoginAsAPIView(generics.GenericAPIView):
	serializer_class = LoginAsInputSerializer

	@transaction.atomic
	def post(self, request, format=None):

		serializer = LoginAsInputSerializer(data=request.data)
		response = {}

		if serializer.is_valid():

			name = serializer.data['name']

			if name == "Super Admin":
				x = PlatformUser.objects.filter(user_type__name = "Super Admin").first()
			else:
				x = PlatformUser.objects.filter(user_type__name = "Content Writer").first()

			token, _ = Token.objects.get_or_create(user=x.user)

			response["result"] = 1
			response["data"] = token.key


		else:
			response["result"] = 0
			response["message"] = ["Please check the input correctly"]

		return Response(response, status=status.HTTP_200_OK)
