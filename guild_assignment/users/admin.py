from django.contrib import admin
from nested_admin import NestedModelAdmin, NestedInlineModelAdmin

from nested_admin import NestedModelAdmin

from .models import *


class UserTypeAdmin(NestedModelAdmin):
	model = UserType

	list_display 	= ['name', 'priority', 'is_deleted', 'created_at', 'updated_at']
    
admin.site.register(UserType, UserTypeAdmin)



class PlatformUserAdmin(NestedModelAdmin):
	model = PlatformUser

	list_display 	= ['uid', 'first_name', 'last_name', 'mobile', 'email', 'designation', 'user_type', 'created_by', 'is_deleted', 'created_at', 'updated_at']
    
admin.site.register(PlatformUser, PlatformUserAdmin)


