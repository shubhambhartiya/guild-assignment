from django.contrib import admin
from django.urls import path, include, re_path
from django.conf.urls import url
	
from rest_framework_swagger.views import get_swagger_view

schema_view = get_swagger_view(title='Guild')

urlpatterns = [
	url(r'^jet/', include('jet.urls', 'jet')),  # Django JET URLS
	url(r'^jet/dashboard/', include('jet.dashboard.urls', 'jet-dashboard')),  # Django JET dashboard URLS
	path('admin/', admin.site.urls),
	url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
	url(r'^nested_admin/', include('nested_admin.urls')),

	re_path(r'^coupons/', include('coupons.urls')),
	re_path(r'^users/', include('users.urls')),
	
    re_path(r'^docs/', schema_view),
]
