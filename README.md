# Guild

This repo represents the backend logic and APIs of the assignment.

## Setup

I would recommend to have this setup in linux based systems, preferably ubuntu, or mac but commands may differ in mac and may require other essentials as well.

A virtual environment needs to be created first to install all the dependencies post which database needs to be imported. Following are the commands
- cd guild-assignment
- virtualenv -p python3.6 .
- source bin/activate
- pip install -r requirements.txt (Mysql server needs to be installed here "sudo apt-get install mysql-server")
- mysql -u root -p guild < guild2.sql  (Assuming root as user, guild as database, guild2.sql is existing database)
- python guild_assignment/manage.py runserver

## APIs used

Go to https://guild.codezen.in/docs
This is the place where APIs can be testing using Open Source Software Framework - Swagger

For the above, a token will be required for authorization which you can get from https://guild.codezen.in/admin
User credentials:
- Username: admin
- Password: guild1234

You will be able to see Tokens inside Auth tokens. Copy any one of them and then again go back to https://guild.codezen.in/docs
Click on Authorize on top right corner and add api key in the manner token {token_copied_from_admin} and Voila!

You can also see the admin using above credentials where all the data sets will be visible. 

For frontend, just visit http://guild_front.codezen.in